from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APITransactionTestCase, APIClient
from django_dynamic_fixture import G, F
from main.models import *


class Endpoints(APITestCase):
    #fixtures = ['seed.json']

    def setUp(self):
        agency = G(Agency, type='agency')
        agencyprofile = G(AgencyProfile, agency=agency)

        client = G(Client, type='client')
        G(ClientProfile, client=client)

        project = G(Project, client=client)
        G(ProjectDeliverable, project=project)
        G(ProjectDeliverable, project=project)
        G(ProjectDeliverable, project=project)

        agency.agencyprofile.assigned_projects.add(project)
        self.client.force_authenticate(user=agency)

    def test_agency_can_retrieve_a_project(self):
        """Agencies can get project details if they are assigned to that project"""
        project = Project.objects.get()
        response = self.client.get('/api/agency/projects/{0}'.format(project.guid))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_agency_can_create_project_proposal(self):
        """Agencies can create a project proposal if they are assigned to that project"""
        project = Project.objects.get()
        project_deliverables = project.projectdeliverable_set.all()
        data = {
            "agency_relevant_experience": "test",
            "cost_estimate": 150000,
            "delivery_date": "2016-06-10",
            "project_deliverable_proposals": [
                {
                    "project_deliverable": project_deliverables[0].id,
                    "cost_estimate": 30000,
                    "delivery_date": "2016-06-07"
                },
                {
                    "project_deliverable": project_deliverables[1].id,
                    "cost_estimate": 20000,
                    "delivery_date": "2016-06-08"
                },
                {
                    "project_deliverable": project_deliverables[2].id,
                    "cost_estimate": 100000,
                    "delivery_date": "2016-06-09"
                }
            ]
        }
        response = self.client.post('/api/agency/projects/{0}/project_proposals'.format(project.guid), data)
        self.assertEqual(response.status_code,status.HTTP_201_CREATED)
