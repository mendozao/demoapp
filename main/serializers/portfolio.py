from rest_framework import serializers
from main.models import Portfolio, PortfolioStock
from main.services.yahoo_stock_service import YahooStockService


class PortfolioStockSerializer(serializers.ModelSerializer):
    stock = serializers.SerializerMethodField()

    class Meta:
        model = PortfolioStock
        fields = ('id', 'stock', 'number_of_shares',)

    def get_stock(self, portfoliostock):
        return YahooStockService.get_stock_info(portfoliostock.stock_ticker)

class PortfolioSerializer(serializers.ModelSerializer):
    stocks = PortfolioStockSerializer(many=True, source='portfoliostock_set')

    class Meta:
        model = Portfolio
        fields = ('id', 'owner', 'stocks',)
