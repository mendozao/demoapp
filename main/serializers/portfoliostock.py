from rest_framework import serializers
from main.models import PortfolioStock, Portfolio


class PortfolioStockCreateSerializer(serializers.ModelSerializer):
    number_of_shares = serializers.IntegerField(required=False)

    class Meta:
        model = PortfolioStock
        fields = ('portfolio', 'stock_ticker', 'number_of_shares',)

    def validate(self, data):
        portfolio = data.get('portfolio')
        stock_ticker = data.get('stock_ticker')
        total_stocks_in_portfolio = portfolio.portfoliostock_set.count()
        if total_stocks_in_portfolio >= 5:
            raise serializers.ValidationError('Max of 5 stocks reached')

        try:
            portfolio.portfoliostock_set.get(stock_ticker=stock_ticker)
            raise serializers.ValidationError('{0} already exists in this portfolio'.format(stock_ticker))
        except PortfolioStock.DoesNotExist:
            pass

        return data
