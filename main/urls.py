from django.conf.urls import url
from main.views.api import *

urlpatterns = [
    # Api endpoints
    url(r'^me/portfolio$', get_portfolio),
    url(r'^me/portfolio/stocks$', add_stock_to_portfolio),
    url(r'^me/portfolio/stocks/(?P<ticker>[a-zA-z]+)$', remove_stock_from_portfolio),
    url(r'^search_stocks/(?P<ticker>[a-zA-z]+)$', search_stocks),
    # url(r'^books$', books),
    # url(r'^books/(?P<id>[0-9]+)$', book),
    # url(r'^reviews/(?P<id>[0-9]+)/upvotes$', upvote),
]
