from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.http import Http404
from django.contrib import messages
from main.models import Portfolio


@require_http_methods(['GET'])
def home(request):
    if request.method == 'GET':
        return render(request, 'main/home.html')

@require_http_methods(['GET', 'POST'])
def login_user_general(request):
    if request.method == 'GET':
        return show_login_or_redirect_to_dashboard(request, login_template_path='main/login.html')
    elif request.method == 'POST':
        return authenticate_and_login_user(request, redirect_url='/signin')

def show_login_or_redirect_to_dashboard(request, **kwargs):
    if request.user.is_authenticated():
        return redirect_user_to_dashboard(request)
    return render(request, kwargs.get('login_template_path'))

def authenticate_and_login_user(request, **kwargs):
    username = request.POST.get('username', '').lower()
    password = request.POST.get('password', '')
    user = authenticate(username=username, password=password)
    if user is not None and user.is_active:
        login(request, user)
        return redirect_user_to_dashboard(request)
    else:
        messages.warning(request, 'Invalid username or password.')
        return redirect(kwargs.get('redirect_url'))

@login_required
def user_dashboard(request):
    try:
        request.user.portfolio
    except Portfolio.DoesNotExist:
        Portfolio.objects.create(owner=request.user)

    return render(request, 'main/dashboard.html')

@login_required
def redirect_user_to_dashboard(request):
    return redirect('/dashboard')

@login_required
def logout_user(request):
    logout(request)
    return redirect('/')
