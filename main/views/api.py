from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import APIException, ValidationError, NotFound
from rest_framework.decorators import api_view
from main.serializers.portfolio import PortfolioSerializer
from main.serializers.portfoliostock import PortfolioStockCreateSerializer
from main.models import Portfolio, PortfolioStock
from main.services.yahoo_stock_service import YahooStockService
from django.db.models import F
import requests


@api_view(['GET'])
def get_portfolio(request):
    return Response(PortfolioSerializer(request.user.portfolio).data, status=status.HTTP_200_OK)


@api_view(['POST'])
def add_stock_to_portfolio(request):
    request.data['portfolio'] = request.user.portfolio.id
    serialized = PortfolioStockCreateSerializer(data=request.data)
    if serialized.is_valid(raise_exception=True):
        portfolio = serialized.save()
        return Response(status=status.HTTP_201_CREATED)


@api_view(['DELETE'])
def remove_stock_from_portfolio(request, ticker):
    try:
        stock = request.user.portfolio.portfoliostock_set.get(stock_ticker=ticker)
        stock.delete()
    except PortfolioStock.DoesNotExist:
        raise NotFound()

    return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def search_stocks(request, ticker):
    quote = YahooStockService.get_stock_info(ticker)
    return Response(quote, status=status.HTTP_200_OK)
