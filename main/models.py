from __future__ import unicode_literals
import uuid
import string, random
from django.db import models
from django.contrib.auth.models import User


class Portfolio(models.Model):
    owner = models.OneToOneField(User)

    def __str__(self):
        return 'Portfolio owned by {0}'.format(self.owner.username)

class PortfolioStock(models.Model):
    portfolio = models.ForeignKey(Portfolio)
    stock_ticker = models.CharField(max_length=5) #might need DB_INDEX, yolo for now
    number_of_shares = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return 'Portfolio Stock Ticker {0}'.format(self.stock_ticker)
