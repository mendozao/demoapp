import requests

class YahooStockService(object):
    def __init__(self):
        pass

    @staticmethod
    def get_stock_info(ticker):
        url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(%22{0}%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='.format(ticker)
        r = requests.get(url)
        # No error handling here on purpose
        return r.json()['query']['results']['quote']
