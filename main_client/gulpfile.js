var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');
var named = require('vinyl-named');
var webpack = require('webpack-stream');
var livereload = require('gulp-livereload');
var webpackConfig = require('./webpack.config.js');
var prdWebpackConfig = require('./webpack.prod.config.js');


// REACT
gulp.task('react-section', function() {
    return gulp.src('./src/react/section/index.jsx')
        .pipe(named(function(file){ return 'section.bundle'; }))
        .pipe(gulpif(process.env.NODE_ENV == 'production', webpack( prdWebpackConfig ) ))
        .pipe(gulpif(process.env.NODE_ENV != 'production', webpack( webpackConfig ) ))
        .pipe(gulp.dest('../build/js'));
});

// SASS
gulp.task('sass-bootstrap', function () {
  gulp.src('./src/scss/bootstrap/bootstrap.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('../build/css'))
    .pipe(gulpif(process.env.NODE_ENV != 'production', livereload() ))
});

gulp.task('sass-components', function () {
  gulp.src('./src/scss/components/components.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('../build/css'))
    .pipe(gulpif(process.env.NODE_ENV != 'production', livereload() ))
});

gulp.task('sass-splash', function () {
  gulp.src('./src/scss/splash.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('../build/css'))
    .pipe(gulpif(process.env.NODE_ENV != 'production', livereload() ))
});

gulp.task('sass-common', function () {
  gulp.src('./src/scss/common.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('../build/css'))
    .pipe(gulpif(process.env.NODE_ENV != 'production', livereload() ))
});

gulp.task('sass-section', function () {
  gulp.src('./src/scss/sections/section.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('../build/css'))
    .pipe(gulpif(process.env.NODE_ENV != 'production', livereload() ))
});

// FONTS
gulp.task('fonts', function () {
  gulp.src(['./src/scss/fonts/*'])
    .pipe(gulp.dest('../build/fonts'));
});

// IMAGES
gulp.task('images', function () {
  gulp.src(['./src/scss/images/*'])
    .pipe(gulp.dest('../build/images'));
});


// DEVELOPMENT
gulp.task('watch-section', function () {
  livereload.listen();
  gulp.watch('./src/scss/**/*.scss', ['sass']);
  gulp.watch('./src/react/**/*.jsx', ['react-section']);
});

gulp.task('watch-sass', function () {
  livereload.listen();
  gulp.watch('./src/scss/**/*.scss', ['sass']);
});

gulp.task('sass', ['sass-bootstrap', 'sass-components', 'sass-splash', 'sass-common', 'sass-section']);
gulp.task('react', ['react-section']);
gulp.task('build', ['sass','fonts','images','react']);
