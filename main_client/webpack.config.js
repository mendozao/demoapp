module.exports = {
  // entry: {
  //   spotlight: './src/lib/react/spotlight/views/ReduxSimpleRouterRoot.jsx',
  //   encoding: './src/lib/react/encoding/index.jsx'
  // },
  output: {
    // path: __dirname + '/wwwroot/dist',
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel'
      },
      {
        test: /\.json?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'json-loader'
      }
    ]
  },
  devtool: 'source-map',
  watch: false
}
