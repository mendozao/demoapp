import React from 'react';
import {PageNav, PageNavItem} from '../../shared/components/PageNav';

export default class Navigation extends React.Component {
  constructor() {
    super();
  }

  render() {
    const {location} = this.props;

    return (
      <PageNav location={location}>
        <PageNavItem page="search" url={`/dashboard`} text="Stock search" />
        <PageNavItem page="portfolio" url={`/dashboard/portfolio`} text="My Portfolio" />
      </PageNav>
    );
  }
}

Navigation.propTypes = {
  location: React.PropTypes.object
};
