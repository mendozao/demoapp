import Immutable from 'immutable';
import { StockSearchActions } from '../actions/StockSearchActions';

const initialState = {
  formData: {
    ticker: '',
    numberOfShares: ''
  },
  stockSearchResult: '',
  meta: {
    isLoading: false
  }
};

export default function stockSearchReducer(state = Immutable.fromJS(initialState), action) {
  switch (action.type) {
    case StockSearchActions.SET_TICKER:
      return state.setIn(['formData', 'ticker'], action.ticker);
    case StockSearchActions.SET_NUMBER_OF_SHARES:
      return state.setIn(['formData', 'numberOfShares'], action.numberOfShares);
    case StockSearchActions.SET_STOCK_SEARCH_RESULT:
      return state.set('stockSearchResult', Immutable.fromJS(action.stock_search_result));
    case StockSearchActions.SET_IS_LOADING:
      return state.setIn(['meta', 'isLoading'], Immutable.fromJS(action.value));
    case StockSearchActions.RESET_VIEW:
      return Immutable.fromJS(initialState);
    default:
      return state;
  }
}
