import {combineReducers} from 'redux';
import stockSearchReducer from './stockSearchReducer';
import portfolioListReducer from './portfolioListReducer';
import alertReducer from '../../shared/reducers/alertReducer';


const portfolioReducer = combineReducers(
  {
    portfolioList: portfolioListReducer,
    stockSearch: stockSearchReducer,
    alert: alertReducer
  }
);

export default portfolioReducer;
