import Immutable from 'immutable';
import { PortfolioListActions } from '../actions/PortfolioListActions';

const initialState = {
  stocks: [],
  meta: {
    isLoading: false
  }
};

export default function portfolioListReducer(state = Immutable.fromJS(initialState), action) {
  switch (action.type) {
    case PortfolioListActions.SET_STOCKS:
      return state.set('stocks', Immutable.fromJS(action.stocks));
    case PortfolioListActions.SET_IS_LOADING:
      return state.setIn(['meta', 'isLoading'], Immutable.fromJS(action.value));
    case PortfolioListActions.RESET_VIEW:
      return Immutable.fromJS(initialState);
    default:
      return state;
  }
}
