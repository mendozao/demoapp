import React from 'react';
import convertToCurrencyFormat from '../helpers/convertToCurrencyFormat';

export default function stockTableConfig(container) {
  return [
    {
      headerName: 'Company Name',
      headerIsClickable: false,
      tdColumnClass: '',
      hideColumnOnSmallScreens: false,
      mapping: (item) => {
        return item.stock.Name;
      }
    },
    {
      headerName: 'Ticker',
      headerIsClickable: false,
      tdColumnClass: '',
      hideColumnOnSmallScreens: false,
      mapping: (item) => {
        return item.stock.Symbol;
      }
    },
    {
      headerName: 'Number of Shares',
      headerIsClickable: false,
      tdColumnClass: '',
      hideColumnOnSmallScreens: false,
      mapping: (item) => {
        return item.number_of_shares;
      }
    },
    {
      headerName: 'Price',
      headerIsClickable: false,
      tdColumnClass: '',
      hideColumnOnSmallScreens: false,
      mapping: (item) => {
        return convertToCurrencyFormat(item.stock.LastTradePriceOnly);
      }
    },
    {
      headerName: 'Market Value',
      headerIsClickable: false,
      tdColumnClass: '',
      hideColumnOnSmallScreens: false,
      mapping: (item) => {
        return convertToCurrencyFormat( (item.stock.LastTradePriceOnly * item.number_of_shares).toFixed(2) );
      }
    },
    {
      headerName: '',
      headerIsClickable: false,
      tdColumnClass: '',
      hideColumnOnSmallScreens: false,
      mapping: (item) => {
        return <a className="btn btn-primary" onClick={e => container.onStockRemoveClicked(item.stock.Symbol)}>Remove</a>;
      }
    }
  ];
}
