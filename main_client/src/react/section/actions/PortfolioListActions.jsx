import React from 'react';
import AppApi from '../../shared/api/AppApi';
const appApi = new AppApi();
import * as AlertActions from '../../shared/actions/AlertActions';


export const PortfolioListActions = {
  SET_STOCKS: 'SET_STOCKS',
  SET_IS_LOADING: 'SET_IS_LOADING',
  ADD_STOCK_TO_PORTFOLIO: 'ADD_STOCK_TO_PORTFOLIO',
  REMOVE_STOCK_TO_PORTFOLIO: 'REMOVE_STOCK_TO_PORTFOLIO'
};

export function setStocks(stocks) {
  return {
    type: PortfolioListActions.SET_STOCKS,
    stocks
  };
}

export function setIsLoading(value) {
  return {
    type: PortfolioListActions.SET_IS_LOADING,
    value
  };
}

export function getPortfolio() {
  return (dispatch, getState) => {
    dispatch( setIsLoading(true) );
    return appApi.getPortfolio()
      .then(response => {
        dispatch( setStocks(response.data.stocks) );
        dispatch( setIsLoading(false) );
      })
      .catch(response => {
        dispatch( setIsLoading(false) );
      });
  };
}

export function addStockToPortfolio(stock) {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      const payload = {
        stock_ticker: stock.ticker
      };

      if(stock.numberOfShares)
        payload.number_of_shares = stock.numberOfShares;

      appApi.addStockToPortfolio(payload)
        .then(response => {
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  };
}

export function removeStockFromPortfolio(ticker) {
  return (dispatch, getState) => {
    return new Promise((resolve, reject) => {
      appApi.removeStockFromPortfolio(ticker)
        .then(response => {
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  };
}
