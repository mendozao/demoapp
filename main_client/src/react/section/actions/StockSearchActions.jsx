import React from 'react';
import AppApi from '../../shared/api/AppApi';
const appApi = new AppApi();


export const StockSearchActions = {
  SET_TICKER: 'SET_TICKER',
  SET_NUMBER_OF_SHARES: 'SET_NUMBER_OF_SHARES',
  SET_STOCK_SEARCH_RESULT: 'SET_STOCK_SEARCH_RESULT',
  SET_IS_LOADING: 'SET_IS_LOADING',
  RESET_VIEW: 'RESET_VIEW'
};

export function setTicker(ticker) {
  return {
    type: StockSearchActions.SET_TICKER,
    ticker: ticker.toString().toUpperCase()
  };
}

export function setNumberOfShares(numberOfShares) {
  return {
    type: StockSearchActions.SET_NUMBER_OF_SHARES,
    numberOfShares
  };
}

export function setStockSearchResult(stock_search_result) {
  return {
    type: StockSearchActions.SET_STOCK_SEARCH_RESULT,
    stock_search_result
  };
}

export function setIsLoading(value) {
  return {
    type: StockSearchActions.SET_IS_LOADING,
    value
  };
}


export function resetView() {
  return {
    type: StockSearchActions.RESET_VIEW
  };
}


export function searchForStock(ticker) {
  return (dispatch, getState) => {
    dispatch( setIsLoading(true) );
    return appApi.getStockInformation(ticker)
      .then(response => {
        dispatch( setStockSearchResult(response.data) );
        dispatch( setIsLoading(false) );
      })
      .catch(response => {
        dispatch( setIsLoading(false) );
      });
  };
}

export function getBooks() {
  return (dispatch, getState) => {
    dispatch( setIsLoading(true) );
    return appApi.getBooks()
      .then(response => {
        dispatch( setBooks(response.data) );
        dispatch( setIsLoading(false) );
      })
      .catch(response => {
        dispatch( setIsLoading(false) );
      });
  };
}
