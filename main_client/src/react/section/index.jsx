import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import {compose, createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider, connect} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import {routerMiddleware, syncHistoryWithStore, routerReducer} from 'react-router-redux';
import portfolioReducer from './reducers/portfolioReducer';
import App from '../shared/containers/App';
import StockSearchContainer from './containers/StockSearchContainer';
import PortfolioListContainer from './containers/PortfolioListContainer';

class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Route path="/dashboard" component={App}>
            <IndexRoute component={StockSearchContainer} />
            <Route path="portfolio" component={PortfolioListContainer} />
          </Route>
        </Router>
      </Provider>
    );
  }
}

const reducer = combineReducers({
  routing: routerReducer,
  app: portfolioReducer
});

const reduxRouteMiddleware = routerMiddleware(browserHistory);

const store = createStore(
  reducer,
  compose(applyMiddleware(reduxRouteMiddleware, thunkMiddleware), window.devToolsExtension ? window.devToolsExtension() : f => f)
);

const history = syncHistoryWithStore(browserHistory, store);

store.subscribe(() => {
  //console.log(store.getState());
});

render((
  <Root />
), document.getElementById('section-container'));
