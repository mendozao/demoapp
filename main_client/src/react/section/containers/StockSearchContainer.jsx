import React from 'react';
import { connect } from 'react-redux';
import { routerActions } from 'react-router-redux';
import Alert from '../../shared/components/Alert';
import CustomDropdown from '../../shared/components/Dropdown';
import LoadingButton from '../../shared/components/LoadingButton';
import Table from '../../shared/components/Table';
import TextArea from '../../shared/components/TextArea';
import TextInput from '../../shared/components/TextInput';
import Navigation from '../components/Navigation';
import { PageNav, PageNavItem } from '../../shared/components/PageNav';
import * as AlertActions from '../../shared/actions/AlertActions';
import * as StockSearchActions from '../actions/StockSearchActions';
import * as PortfolioListActions from '../actions/PortfolioListActions';

class StockSearchContainer extends React.Component {
  constructor() {
    super();
  }

  renderSearchResult() {
    const { dispatch, stockSearch } = this.props;

    if(!stockSearch.stockSearchResult)
      return null;

    if(!stockSearch.stockSearchResult.Name)
      return 'No matches';

    return [<div>Search Result</div>, <div className="well">
      <div>{stockSearch.stockSearchResult.Name}</div>
      <div>{stockSearch.stockSearchResult.Symbol}</div>
      <div>{stockSearch.stockSearchResult.LastTradePriceOnly}</div>
      <div>
        <TextInput
          placeholder="Enter number of shares"
          value={stockSearch.formData.numberOfShares}
          onBlur={value => dispatch( StockSearchActions.setNumberOfShares(value) )} />
      </div>

      <a className="btn btn-primary" onClick={e => {
        dispatch( PortfolioListActions.setIsLoading(true) );
        dispatch( PortfolioListActions.addStockToPortfolio(stockSearch.formData) )
        .then(response => {
          dispatch( PortfolioListActions.setIsLoading(false) );
          dispatch( StockSearchActions.resetView() );
          dispatch( AlertActions.displaySuccessAlert(`Successfully added ${stockSearch.formData.ticker} to your portfolio`) );
        })
        .catch(response => {
          if(response.data.non_field_errors)
            dispatch( AlertActions.displayFailureAlert(response.data.non_field_errors[0]) );
          else
            dispatch( AlertActions.displayFailureAlert('Something went wrong') );
          dispatch( PortfolioListActions.setIsLoading(false) );
        })
      }}>Add to portfolio</a>
    </div>];
  }

  render() {
    const { dispatch, stockSearch, alert } = this.props;

    return (
      <div className="padded-container">
        <Alert {...alert} onClose={() => dispatch( AlertActions.closeAlert() )}/>
        <Navigation location={location}/>
        <div className="row col-md-12">
          <h3>Search stocks</h3>
          <TextInput
            label="Ticker"
            placeholder="Enter a stock ticker"
            value={stockSearch.formData.ticker}
            onBlur={value => dispatch( StockSearchActions.setTicker(value) )} />

          <LoadingButton
            className='btn-primary pull-right'
            loading={stockSearch.meta.isLoading}
            loadingText='Searching stocks...'
            onClick={e => dispatch( StockSearchActions.searchForStock(stockSearch.formData.ticker) ) } >
            Search
          </LoadingButton>
        </div>

        <div className="row col-md-12">
          {this.renderSearchResult()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    stockSearch: state.app.stockSearch.toJS(),
    alert: state.app.alert.toJS()
  };
}

export default connect(mapStateToProps)(StockSearchContainer);
