import React from 'react';
import { connect } from 'react-redux';
import { routerActions } from 'react-router-redux';
import Alert from '../../shared/components/Alert';
import CustomDropdown from '../../shared/components/Dropdown';
import LoadingButton from '../../shared/components/LoadingButton';
import Table from '../../shared/components/Table';
import TextArea from '../../shared/components/TextArea';
import TextInput from '../../shared/components/TextInput';
import Navigation from '../components/Navigation';
import { PageNav, PageNavItem } from '../../shared/components/PageNav';
import * as AlertActions from '../../shared/actions/AlertActions';
import * as PortfolioListActions from '../actions/PortfolioListActions';
import stockTableConfig from '../tables/stockTableConfig';

class PortfolioContainer extends React.Component {
  constructor() {
    super();
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch( PortfolioListActions.getPortfolio() );

    this.timerId = setInterval(() => {
      dispatch( PortfolioListActions.getPortfolio() );
    }, 5000);
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    clearInterval(this.timerId);
  }

  onStockRemoveClicked(ticker) {
    const { dispatch } = this.props;
    dispatch( PortfolioListActions.removeStockFromPortfolio(ticker) )
      .then(response => {
        dispatch( PortfolioListActions.getPortfolio() );
        dispatch( AlertActions.displaySuccessAlert(`Successfully removed ${ticker} from your portfolio`) );
      })
      .catch(response => {
        dispatch( AlertActions.displayFailureAlert('Something went wrong') );
      });
  }

  renderStocks() {
    const { dispatch, portfolioList } = this.props;

    if(portfolioList.stocks.length === 0)
      return <div className="display-no-content">No stocks in your portfolio</div>;
    return <Table
        config={stockTableConfig(this)}
        items={portfolioList.stocks}
        rowClassNames={{'animated fadeIn': (item) => { return true; }}} />;
  }

  render() {
    const { dispatch, alert } = this.props;

    return (
      <div className="padded-container">
        <Alert {...alert} onClose={() => dispatch( AlertActions.closeAlert() )}/>
        <Navigation location={location}/>
        <div className="row col-md-12">
          <h3>My Portfolio</h3>
        </div>

        <div className="row col-md-12">
          {this.renderStocks()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    portfolioList: state.app.portfolioList.toJS(),
    alert: state.app.alert.toJS()
  };
}

export default connect(mapStateToProps)(PortfolioContainer);
