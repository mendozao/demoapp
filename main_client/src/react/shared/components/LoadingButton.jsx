import React from 'react';
import Button from './Button';


export default class LoadingButton extends React.Component {
  constructor() {
    super();
  }

  clickButton(e) {
    if (this.props.onClick)
      this.props.onClick(e);
  }

  isDisabled() {
    if (this.props.disabled)
      return true;

    return this.props.loading;
  }

  render() {
    const {type, className, loading, loadingText, children} = this.props;

    const loadingMarkup = <div className="loading-markup animated zoomIn">{loadingText}<img src="/static/images/loader.svg" height="19px" width="19px"/></div>

    return (
      <Button
        type={type}
        className={className}
        disabled={this.isDisabled()}
        onClick={e => this.clickButton(e)}>{(loading ? loadingMarkup : children)}</Button>
    );
  }
}

LoadingButton.propTypes = {
  type: React.PropTypes.string,
  className: React.PropTypes.string,
  disabled: React.PropTypes.bool,
  onClick: React.PropTypes.func,
  loading: React.PropTypes.bool.isRequired,
  loadingText: React.PropTypes.string
};
