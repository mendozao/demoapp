import React from 'react';
import {Link} from 'react-router';

export class PageNav extends React.Component {
  render() {
    var children = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, { pathname: this.props.location ? this.props.location.pathname : '' });
    });

    return (
      <ul className="nav-pills">
        {children}
      </ul>
    );
  }
}

PageNav.propTypes = {
  location: React.PropTypes.object.isRequired
};

export class PageNavItem extends React.Component {
  render() {
    return (
      <li className={(this.props.url === this.props.pathname) ? 'active' : ''}>
        <Link to={this.props.url}>{this.props.text}</Link>
      </li>
    );
  }
}

PageNavItem.propTypes = {
  url: React.PropTypes.string.isRequired,
  text: React.PropTypes.string.isRequired
};
