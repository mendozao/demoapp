import React from 'react';

export default class Table extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <table className={`table ${this.props.isLoading ? 'table-loading':''}`}>
        <thead>
          <tr>
            {
              this.props.config.map( (configObject, i) => {
                const thClassName = (configObject.hideColumnOnSmallScreens ? 'hidden-xs ' : '') + (configObject.tdColumnClass || '');
                if(configObject.headerIsClickable) {
                  if(this.props.sortColumn && this.props.sortColumn === configObject.headerName) {
                    if(this.props.sortType === 'ASCENDING')
                      return <th className={thClassName} key={i}><a onClick={e => this.props.onHeaderClicked(configObject.headerName)}>{configObject.headerName} Down</a></th>;
                    else if(this.props.sortType === 'DESCENDING')
                      return <th className={thClassName} key={i}><a onClick={e => this.props.onHeaderClicked(configObject.headerName)}>{configObject.headerName} Up</a></th>;
                  }
                  else
                    return <th className={thClassName} key={i}><a onClick={e => this.props.onHeaderClicked(configObject.headerName)}>{configObject.headerName}</a></th>;
                }
                return <th className={thClassName} key={i}>{configObject.headerName}</th>;
              })
            }
          </tr>
        </thead>
        <tbody>
          {
            this.props.items.map( (item, i) => {
              const column = this.props.config.map((configObject, i) => {
                const tdClassName = (configObject.hideColumnOnSmallScreens ? 'hidden-xs ' : '') + (configObject.tdColumnClass || '');
                return <td className={tdClassName} key={i}>{configObject.mapping(item)}</td>;
              });

              // Allow the user to pass in an object, rowClassNames, with keys equal to the css class that the
              // user wants to add to the <tr> - and a function that tests whether or not to
              // apply the given class.
              let rowClassNames;
              if (this.props.rowClassNames) {
                rowClassNames = Object.keys(this.props.rowClassNames).map((className) => {
                  if(this.props.rowClassNames[className](item) === true)
                    return className;
                  return null;
                }).join(' ');
              }
              return <tr className={rowClassNames} key={i}>{column}</tr>;
            })
          }
        </tbody>
      </table>
    );
  }
}

Table.propTypes = {
  items: React.PropTypes.array.isRequired,
  config: React.PropTypes.arrayOf(React.PropTypes.shape({
    headerName: React.PropTypes.string.isRequired,
    headerIsClickable: React.PropTypes.bool.isRequired,
    tdColumnClass: React.PropTypes.string,
    hideColumnOnSmallScreens:  React.PropTypes.bool,
    mapping: React.PropTypes.func.isRequired
  })),
  onHeaderClicked: React.PropTypes.func,
  sortColumn: React.PropTypes.string,
  sortType: React.PropTypes.oneOf([ 'ASCENDING', 'DESCENDING' ]),
  isLoading: React.PropTypes.bool,
  // Object keys map to a function - if function returns true, a class equal to object key is added to table row
  // {'my-custom-class': function(someVar){ return true; }}
  rowClassNames: React.PropTypes.object
};
