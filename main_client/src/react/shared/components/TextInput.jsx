import React from 'react';

export default class TextInput extends React.Component {
  constructor() {
    super();
    this.state = {
      text: ''
    };
  }

  componentDidMount() {
    if(this.props.value)
      this.setState({text: this.props.value});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.text) {
      this.setState({text: nextProps.value});
    }
  }

  handleChange(value) {
    this.setState({text: value});
    if(this.props.onChange)
      this.props.onChange(value);
  }

  onBlur(value) {
    if (this.props.onBlur)
      this.props.onBlur(value);
  }

  render() {
    const hasError = this.props.error ? 'has-error' : '';
    const className = `form-group ${hasError}`;

    let spanError;
    if(this.props.error && !this.props.hideErrorMessage)
      spanError = <span className="help-block animated fadeIn">{this.props.error}</span>;

    let input;
    if(this.props.type === 'number') {
      input = <div className="input-group">
          <span className="input-group-addon">$</span>
          <input
            type={this.props.type}
            className="form-control"
            placeholder={this.props.placeholder}
            value={this.state.text}
            onChange={e => this.handleChange(e.target.value)}
            onBlur={e => this.onBlur(e.target.value)} />
        </div>;
    }else {
      input = <input
        type={this.props.type}
        className="form-control"
        placeholder={this.props.placeholder}
        value={this.state.text}
        onChange={e => this.handleChange(e.target.value)}
        onBlur={e => this.onBlur(e.target.value)} />;
    }

    return (
      <div className={className}>
        <label className="control-label">{this.props.label}</label>
        {input}
        {spanError}
      </div>
    );
  }
}

TextInput.propTypes = {
  label: React.PropTypes.string,
  type:  React.PropTypes.oneOf([ 'text', 'password', 'number' ]),
  placeholder: React.PropTypes.string,
  onBlur: React.PropTypes.func,
  onChange: React.PropTypes.func,
  error: React.PropTypes.array,
  hideErrorMessage: React.PropTypes.bool
};

TextInput.defaultProps = { type: 'text', hideErrorMessage: false };
