import React from 'react';

export default class Alert extends React.Component {
  constructor() {
    super();
  }

  onClose() {
    if (this.props.onClose)
      this.props.onClose();
  }

  render() {
    if(this.props.show)
      return (
        <div className={`alert alert-${this.props.type} animated fadeIn`} role="alert">
          <button type="button" className="close" aria-label="Close" onClick={e => this.onClose()}><span aria-hidden="true">&times;</span></button>
          {this.props.message}
        </div>
      );
    return null;
  }
}

Alert.propTypes = {
  onClose: React.PropTypes.func.isRequired,
  show: React.PropTypes.bool.isRequired,
  message: React.PropTypes.string.isRequired,
  type: React.PropTypes.oneOf(['success', 'danger'])
};
