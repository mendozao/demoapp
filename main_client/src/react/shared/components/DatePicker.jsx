import React from 'react';
import DatePicker from 'react-datepicker';

export default class DatePickerModified extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className={`form-group ${this.props.error ? 'has-error' : ''}`}>
        <label className="control-label">{this.props.label}</label>
        <DatePicker {...this.props} />
      </div>
    );
  }
}

DatePickerModified.propTypes = {
  label: React.PropTypes.string,
  error: React.PropTypes.array
};
