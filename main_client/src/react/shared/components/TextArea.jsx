import React from 'react';

export default class TextArea extends React.Component {
  constructor() {
    super();
    this.state = {
      text: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.text) {
      this.setState({text: nextProps.value});
    }
  }

  handleChange(value) {
    this.setState({text: value});
    if(this.props.onChange)
      this.props.onChange(value);
  }

  onBlur(value) {
    if (this.props.onBlur)
      this.props.onBlur(value);
  }

  render() {
    const hasError = this.props.error ? 'has-error' : '';
    const className = `form-group ${hasError}`;

    let spanError;
    if(this.props.error)
      spanError = <span className="help-block animated fadeIn">{this.props.error}</span>;

    return (
      <div className={className}>
        <label className="control-label">{this.props.label}</label>
        <textarea
          rows={this.props.rows}
          type={this.props.type}
          className="form-control"
          placeholder={this.props.placeholder}
          value={this.state.text}
          onChange={e => this.handleChange(e.target.value)}
          onBlur={e => this.onBlur(e.target.value)} />
        {spanError}
      </div>
    );
  }
}

TextArea.propTypes = {
  label: React.PropTypes.string,
  type:  React.PropTypes.oneOf([ 'text', 'password' ]),
  placeholder: React.PropTypes.string,
  onBlur: React.PropTypes.func,
  onChange: React.PropTypes.func,
  error: React.PropTypes.array
};

TextArea.defaultProps = { type: 'text' };
