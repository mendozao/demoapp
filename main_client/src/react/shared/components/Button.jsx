import React from 'react';

export default class Button extends React.Component {
  constructor() {
    super();
  }

  clickButton(e) {
    if (!this.props.disabled && this.props.onClick)
      this.props.onClick(e);
  }

  getClassName() {
    const {className, disabled} = this.props;

    let buttonClassName = 'btn';

    if (className)
      buttonClassName += ' ' + className;

    if (disabled)
      buttonClassName += ' disabled';

    return buttonClassName;
  }

  render() {
    const { type, className, children, disabled} = this.props;

    return (
      <button type={type} className={this.getClassName()} onClick={e => this.clickButton(e)}>
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  type: React.PropTypes.string,
  className: React.PropTypes.string,
  disabled: React.PropTypes.bool,
  onClick: React.PropTypes.func
};
