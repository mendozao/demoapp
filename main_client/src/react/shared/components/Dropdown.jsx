import React from 'react';
import Select from 'react-select-plus';


export default class CustomDropdown extends React.Component {
  constructor() {
    super();
  }

  onChange(option) {
    // Prefer sending an empty string as a default value
    if(option === undefined || option === null)
      option = '';
    this.props.onChange(option);
  }

  render() {
    const hasError = this.props.error ? 'has-error' : '';
    const className = `form-group ${hasError}`;

    let spanError;
    if(this.props.error)
      spanError = <span className="help-block animated fadeIn">{this.props.error}</span>;

    return (
      <div className={className}>
        <label className="control-label">{this.props.label}</label>
        <Select
          clearable={true}
          searchable={false}
          backspaceRemoves={false}
          placeholder={this.props.placeholder}
          options={this.props.options}
          onChange={option => this.onChange(option)}
          value={this.props.value}
          disabled={this.props.disabled} />
        {spanError}
      </div>
    );
  }
}

CustomDropdown.propTypes = {
  label: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  options:  React.PropTypes.array,
  value: React.PropTypes.object,
  onChange: React.PropTypes.func,
  error: React.PropTypes.string,
  disabled: React.PropTypes.bool
};
