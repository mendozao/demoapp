import Immutable from 'immutable';
import { AlertActions } from '../actions/AlertActions';

const initialState = {
  show: false,
  message: '',
  type: ''
};

export default function alertReducer(state = Immutable.fromJS(initialState), action) {
  switch (action.type) {
    case AlertActions.DISPLAY_SUCCESS_ALERT:
      return state.mergeDeep({show: true, message: action.message, type: 'success'});
    case AlertActions.DISPLAY_FAILURE_ALERT:
      return state.mergeDeep({show: true, message: action.message, type: 'danger'});
    case AlertActions.CLOSE_ALERT:
      return Immutable.fromJS(initialState);
    default:
      return state;
  }
}
