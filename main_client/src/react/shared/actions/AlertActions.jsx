export const AlertActions = {
  DISPLAY_SUCCESS_ALERT: 'DISPLAY_SUCCESS_ALERT',
  DISPLAY_FAILURE_ALERT: 'DISPLAY_FAILURE_ALERT',
  CLOSE_ALERT: 'CLOSE_ALERT'
};


export function displaySuccessAlert(message) {
  return {
    type: AlertActions.DISPLAY_SUCCESS_ALERT,
    message
  };
}

export function displayFailureAlert(message) {
  return {
    type: AlertActions.DISPLAY_FAILURE_ALERT,
    message
  };
}

export function closeAlert() {
  return {
    type: AlertActions.CLOSE_ALERT
  };
}
