import axios from 'axios';
import getCookie from '../utils/getCookie';
axios.defaults.headers.common['X-CSRFToken'] = getCookie('csrftoken');

export default class AppApi {

  getStockInformation(ticker) {
    return axios.get(`/api/search_stocks/${ticker}`);
  }

  getPortfolio() {
    return axios.get('/api/me/portfolio');
  }

  addStockToPortfolio(stock) {
    return axios.post('/api/me/portfolio/stocks', stock);
  }

  removeStockFromPortfolio(ticker) {
    return axios.delete(`/api/me/portfolio/stocks/${ticker}`);
  }
}
