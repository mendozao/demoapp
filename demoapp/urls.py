from django.conf.urls import url, include
from django.contrib import admin
from main.views.pages import *


urlpatterns = [
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('main.urls')),
    url(r'^$', home),
    url(r'^signin$', login_user_general),
    url(r'^logout$', logout_user),
    url(r'^dashboard$', user_dashboard),
]
